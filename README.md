# README #

## What is this repository for? ##

Software for sample collector device. 
The device will be responsible for collecting data from the test system. 
The data that will be processed include voltage or light intensity.



## The development system is based on 
* GCC cross compiler
* Make for Windows
* OpenOCD
* Git for Windows
* STM32CubeMX
* Visual Studio Code
* Cortex-Debug (VSC extension)

### Tools to download(windows) ###

Tools helpful for develop software for STM32 uC.

1. Compiler: arm-gcc [*.zip]
	* link: https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads
	* version: 10.3.2021.07
	* features: compiler

2. Make for windows (Binaries and Dependencies) [*.zip]
	* link http://gnuwin32.sourceforge.net/packages/make.htm
	* version: 3.81
	* features: support for Makefiles

3. openocd [*.zip]
	* link: https://gnutoolchains.com/arm-eabi/openocd/
	* version: 20210729
	* features: programing uC by SWD

4. Git for windows:
	* link: https://git-scm.com/download/win
	* version: 2.34.0
	* features: git and set of popular linux command for windows(of course You can live without that)

5. Software generates for STM32: STM32CubeMX
	* link: https://www.st.com/en/development-tools/stm32cubemx.html
	* version: 6.3.0
	* features: generate low-level code through the grapical interface

6. Visual Studio Code: 
	* link: https://code.visualstudio.com/download
	* version: 1.62.3
	* feature: development environment for C 

7. Cortex-Debug (VSC extension):
	* link: https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug
	* version: 0.4.10
	* feature: debugging STM32 via Visual Studio Code

### Tools installation guide(windows) ####

1. Download all files 
2. Unpack arm-gcc, openocd and git in specifc location e.g `C:\dev\tools\`
3. Unpack make-bin and make-dep in specifc location e.g `C:\dev\tools\make-3.81`
4. To environment variable PATH add:
	* `C:\dev\tools\make-3.81\bin`
	* `C:\dev\tools\git\bin`
	* `C:\dev\tools\git\usr\bin`
	* `C:\dev\tools\OpenOCD-20210729-0.11.0\bin`
	* `C:\dev\tools\gcc-arm-none-eabi-10.3-2021.07\bin`
5. Test all tools for following commands in CMD: 
	* make -version 
	* openocd --version
	* arm-none-eabi-gcc --version 
	* git --version

For more: https://www.youtube.com/watch?v=PxQw5_7yI8Q

### Using visual studio code on windows ###

1. Install Visual Studio Code 
1. Create environment variable: `ARMGCC_DIR` with value: `C:\dev\tools\gcc-arm-none-eabi-10.3-2021.07\bin`
2. In repository in folder `.vscode` is proper configuration: 
	* [c_cpp_properties.json](.vscode/c_cpp_properties.json): Thanks to this file IDE can help with develop, expanding function name, class field etc.
	* [launch.json](.vscode/launch.json): debug configuration
3. Extensions (required or useful):
	* C/C++:
	* Cortex-Debug: 
4. Usage:
	* Debug session: Run -> Start Debugging (F5)
	* Load firmware: Run -> Run without Debugging (Ctrl + F5)
	* Of course you can just call `make flash` in proper folder using terminal embedded in VsC

For more: https://www.youtube.com/watch?v=PxQw5_7yI8Q

### Flashing target(windows) with openocd and stlink v2 ###

1. Adding target to flash via openocd in Makefile - https://openocd.org/doc/html/Flash-Programming.html for example:  
flash: all  
	openocd -f interface/stlink-v2.cfg -f target/stm32f4x.cfg -c "program $(BUILD_DIR)/$(TARGET).elf verify reset exit"   
2. Compile program 
3. Run created target, for example: make flash

### Prepare repository ###
It is important to update submodules!

1. `git clone git@bitbucket.org:tnowicki2/nrf5x_tests.git`
2. `git submodule init`
3. `git submodule update`
	* look file `.gitmodules`

For more: https://git-scm.com/book/en/v2/Git-Tools-Submodules

	

